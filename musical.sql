--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 9.6.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE ar_internal_metadata OWNER TO postgres;

--
-- Name: artista; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE artista (
    id_genero integer,
    nome character varying,
    thumb character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    id_artista integer NOT NULL
);


ALTER TABLE artista OWNER TO postgres;

--
-- Name: artista_id_artista_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE artista_id_artista_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE artista_id_artista_seq OWNER TO postgres;

--
-- Name: artista_id_artista_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE artista_id_artista_seq OWNED BY artista.id_artista;


--
-- Name: favorita; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE favorita (
    id integer NOT NULL,
    id_musica integer,
    id_user integer,
    ordem integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE favorita OWNER TO postgres;

--
-- Name: favorita_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE favorita_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE favorita_id_seq OWNER TO postgres;

--
-- Name: favorita_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE favorita_id_seq OWNED BY favorita.id;


--
-- Name: generos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE generos (
    nome character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    id_genero integer NOT NULL
);


ALTER TABLE generos OWNER TO postgres;

--
-- Name: generos_id_genero_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE generos_id_genero_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE generos_id_genero_seq OWNER TO postgres;

--
-- Name: generos_id_genero_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE generos_id_genero_seq OWNED BY generos.id_genero;


--
-- Name: musicas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE musicas (
    id_artista integer,
    nome character varying,
    caminho character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    id_musica integer NOT NULL
);


ALTER TABLE musicas OWNER TO postgres;

--
-- Name: musicas_id_musica_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE musicas_id_musica_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE musicas_id_musica_seq OWNER TO postgres;

--
-- Name: musicas_id_musica_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE musicas_id_musica_seq OWNED BY musicas.id_musica;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE schema_migrations OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    nome character varying,
    email character varying,
    idade integer,
    password character varying,
    password_diggest character varying,
    password_confirmation character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    id_user integer NOT NULL,
    password_digest character varying
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: users_id_user_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_user_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_user_seq OWNER TO postgres;

--
-- Name: users_id_user_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_user_seq OWNED BY users.id_user;


--
-- Name: artista id_artista; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY artista ALTER COLUMN id_artista SET DEFAULT nextval('artista_id_artista_seq'::regclass);


--
-- Name: favorita id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY favorita ALTER COLUMN id SET DEFAULT nextval('favorita_id_seq'::regclass);


--
-- Name: generos id_genero; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY generos ALTER COLUMN id_genero SET DEFAULT nextval('generos_id_genero_seq'::regclass);


--
-- Name: musicas id_musica; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY musicas ALTER COLUMN id_musica SET DEFAULT nextval('musicas_id_musica_seq'::regclass);


--
-- Name: users id_user; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id_user SET DEFAULT nextval('users_id_user_seq'::regclass);


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2017-02-28 20:37:23.176465	2017-02-28 20:37:23.176465
\.


--
-- Data for Name: artista; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY artista (id_genero, nome, thumb, created_at, updated_at, id_artista) FROM stdin;
1	Charlie Brown Jr	http://i.imgur.com/5o1KOhd.png	2017-02-28 20:41:27.768431	2017-02-28 20:41:27.768431	1
1	Capital Inicial	http://i.imgur.com/PrFS4ru.jpg	2017-02-28 20:41:27.772432	2017-02-28 20:41:27.772432	2
1	Engenheiros do Hawaii	https://studiosol-a.akamaihd.net/letras/250x250/albuns/3/e/5/0/59491441052487.jpg	2017-02-28 20:41:27.774462	2017-02-28 20:41:27.774462	3
1	Legião Urbana	https://upload.wikimedia.org/wikipedia/pt/thumb/e/ec/Legi%C3%A3o_Urbana_-_Mais_do_Mesmo.jpg/220px-Legi%C3%A3o_Urbana_-_Mais_do_Mesmo.jpg	2017-02-28 20:41:27.776467	2017-02-28 20:41:27.776467	4
2	Raça Negra	http://2.bp.blogspot.com/-lbTzWRJk9vU/Uuk2xD7Q4dI/AAAAAAADjng/to4jVfLivDs/s1600/raca-negra4.jpg	2017-02-28 20:41:27.779435	2017-02-28 20:41:27.779435	5
2	Katinguele	http://www.kboing.com.br/fotos/imagens/4a09c67768e8d.jpg	2017-02-28 20:41:27.782436	2017-02-28 20:41:27.782436	6
2	Art Popular	http://cdn.letras.com.br/arquivos/fotos/capas/188/0018782,ao-vivo-sem-abuso.jpg	2017-02-28 20:41:27.786438	2017-02-28 20:41:27.786438	7
2	Só pra contrariar	https://studiosol-a.akamaihd.net/letras/250x250/albuns/a/6/4/2/16481.jpg	2017-02-28 20:41:27.790438	2017-02-28 20:41:27.790438	8
\.


--
-- Name: artista_id_artista_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('artista_id_artista_seq', 1, false);


--
-- Data for Name: favorita; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY favorita (id, id_musica, id_user, ordem, created_at, updated_at) FROM stdin;
58	2	1	0	2017-03-01 02:52:18.377163	2017-03-01 02:52:18.377163
60	7	1	0	2017-03-01 02:56:47.308531	2017-03-01 02:56:47.308531
62	9	1	0	2017-03-01 02:57:25.454694	2017-03-01 02:57:25.454694
63	8	1	0	2017-03-01 02:58:02.560547	2017-03-01 02:58:02.560547
38	\N	1	0	2017-02-28 23:24:17.448472	2017-02-28 23:24:17.448472
54	1	1	0	2017-02-28 23:54:23.578171	2017-02-28 23:54:23.578171
55	4	1	0	2017-02-28 23:54:39.312179	2017-02-28 23:54:39.312179
\.


--
-- Name: favorita_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('favorita_id_seq', 63, true);


--
-- Data for Name: generos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY generos (nome, created_at, updated_at, id_genero) FROM stdin;
Pop/ROCK Nacional	2017-02-28 20:41:27.729461	2017-02-28 20:41:27.729461	1
Pagode Anos 90	2017-02-28 20:41:27.755427	2017-02-28 20:41:27.755427	2
\.


--
-- Name: generos_id_genero_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('generos_id_genero_seq', 1, false);


--
-- Data for Name: musicas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY musicas (id_artista, nome, caminho, created_at, updated_at, id_musica) FROM stdin;
1	Só os Loucos Sabem	/assets/musicas/Charlie Brown Jr. - Só os Loucos Sabem.mp3	2017-02-28 20:41:27.801444	2017-02-28 20:41:27.801444	1
1	Céu Azul	/assets/musicas/Charlie Brown Jr. - Céu Azul (Audio Perfeito).mp3	2017-02-28 20:41:27.806445	2017-02-28 20:41:27.806445	2
2	O Mundo	/assets/musicas/capital-inicial/02 - O Mundo.mp3	2017-02-28 20:41:27.80945	2017-02-28 20:41:27.80945	3
2	Natasha	/assets/musicas/capital-inicial/12 - Natasha.mp3	2017-02-28 20:41:27.812492	2017-02-28 20:41:27.812492	4
3	Era Um Garoto Que Como Eu Amava Os Beatles E Os Rolling Stones	/assets/musicas/Era Um Garoto Que Como Eu Amava Os Beatles E Os Rolling Stones.mp3	2017-02-28 20:41:27.818449	2017-02-28 20:41:27.818449	5
4	Eduardo e Mônica	/assets/musicas/Legião Urbana - Eduardo e Monica.mp3	2017-02-28 20:41:27.815454	2017-02-28 20:41:27.815454	6
5	Cheia de Manias	/assets/musicas/Raça Negra - Cheia de Manias (Letra).mp3	2017-02-28 20:41:27.820471	2017-02-28 20:41:27.820471	7
6	Lua Vai	/assets/musicas/Katinguele - Lua Vai.mp3	2017-02-28 20:41:27.823473	2017-02-28 20:41:27.823473	8
7	Art Popular - Pimpolho	/assets/musicas/Art Popular - Pimpolho.mp3	2017-02-28 20:41:27.825542	2017-02-28 20:41:27.825542	9
8	Que Se Chama Amor	/assets/musicas/Só Pra Contrariar - Que Se Chama Amor.mp3	2017-02-28 20:41:27.828455	2017-02-28 20:41:27.828455	10
\.


--
-- Name: musicas_id_musica_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('musicas_id_musica_seq', 10, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY schema_migrations (version) FROM stdin;
20170226183937
20170226183938
20170226183939
20170226183941
20170226184915
20170228072526
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (nome, email, idade, password, password_diggest, password_confirmation, created_at, updated_at, id_user, password_digest) FROM stdin;
Adriano Belisario	adriano0488@gmail.com	\N	\N	\N	\N	2017-02-28 22:36:26.699326	2017-02-28 22:36:26.699326	1	$2a$10$DxKEszp4wJI6OezmWb7Q9ufoLmu5sU5aGLzyb/7CYL8uvIK2oz1w2
\.


--
-- Name: users_id_user_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_user_seq', 1, true);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: artista artista_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY artista
    ADD CONSTRAINT artista_pkey PRIMARY KEY (id_artista);


--
-- Name: favorita favorita_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY favorita
    ADD CONSTRAINT favorita_pkey PRIMARY KEY (id);


--
-- Name: generos generos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY generos
    ADD CONSTRAINT generos_pkey PRIMARY KEY (id_genero);


--
-- Name: musicas musicas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY musicas
    ADD CONSTRAINT musicas_pkey PRIMARY KEY (id_musica);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id_user);


--
-- Name: artista fk_artista_genero; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY artista
    ADD CONSTRAINT fk_artista_genero FOREIGN KEY (id_genero) REFERENCES generos(id_genero) ON DELETE CASCADE;


--
-- Name: favorita fk_favorita_musica; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY favorita
    ADD CONSTRAINT fk_favorita_musica FOREIGN KEY (id_musica) REFERENCES musicas(id_musica) ON DELETE CASCADE;


--
-- Name: favorita fk_favorita_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY favorita
    ADD CONSTRAINT fk_favorita_user FOREIGN KEY (id_user) REFERENCES users(id_user) ON DELETE CASCADE;


--
-- Name: musicas fk_musicas_artista; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY musicas
    ADD CONSTRAINT fk_musicas_artista FOREIGN KEY (id_artista) REFERENCES artista(id_artista) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

