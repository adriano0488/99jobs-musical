function favoritarMusicas(id_musica,elemento){

            $.ajax({
                url:'/ajax/favoritar',
                data:{
                    id_musica:id_musica

                },
                beforeSend:function(){

                },success:function(result){
                    if(result.success == true){
                        var type = 'success';
                        var message = result.message;
                    }else{
                        var type = 'warning';
                        if(result.message != undefined){
                            var message = result.message;
                        }else{
                            var message = 'Erro ao favoritar música';
                        }

                    }
                    var svg = $(elemento).find('svg');

                    if(result.favoritado == true){
                        svg.addClass('albuns__heart--favorited');
                    } else {
                        svg.removeClass('albuns__heart--favorited');
                    }

                    $.toast({
                        heading: '',
                        text: message,
                        position: 'top-right',
                        showHideTransition: 'slide',
                        icon: type
                    })

                }

            }).fail(function(error){
                $.toast({
                        heading: '',
                        text: 'Ocorreu um erro ao favoritar/desfavoritar música',
                        showHideTransition: 'slide',
                        position: 'top-right',
                        icon: 'warning'
                    })

            });

            
        }


            $(function(){
                var legends = $('footer .legends h2');
                // Setup the player to autoplay the next track
                var a = audiojs.createAll({
                    trackEnded: function() {
                    var next = $('.block-musica.playing').next();
                    if (!next.length) next = $('.block-musica').first();
                        next.addClass('playing').siblings().removeClass('playing');
                        audio.load($('a', next).attr(next.data('musica').caminho));
                        audio.play();
                    }
                });
              
                // Load in the first track
                var audio = a[0];

                if(typeof(audio) != 'undefined'){
                    var block_musica = $('.block-musica');
                    var data_musica = block_musica.attr('data-musica');
                    if(data_musica != undefined){
                        data_musica = JSON.parse(data_musica);
                        first = data_musica.caminho;
                        var data_artista = block_musica.attr('data-artista');
                        data_artista = JSON.parse(data_artista);

                        legends.text(data_artista.nome + ' - ' + data_musica.nome);
                        block_musica.first().addClass('playing');
                        audio.load(first);
                    }
                    

                    // Carregar musica atraves do clique
                    $('.block-musica button').click(function(e) {
                        e.preventDefault();
                        var block_musica = $(this).closest('.block-musica');
                        var data_musica = block_musica.attr('data-musica');
                        data_musica = JSON.parse(data_musica);

                        var data_artista = block_musica.attr('data-artista');
                        data_artista = JSON.parse(data_artista);

                        legends.text(data_artista.nome + ' - ' + data_musica.nome);

                        block_musica.addClass('playing').siblings().removeClass('playing');
                        audio.load(data_musica.caminho);
                        audio.play();
                    });
                }
            });