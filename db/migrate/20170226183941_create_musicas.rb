class CreateMusicas < ActiveRecord::Migration[5.0]

  
    def change
    create_table :musicas, :primary_key => :id_musica,:id=>false do |t|
        #t.integer :id_musica
        t.integer :id_artista
        t.string :nome
        t.string :caminho
        t.timestamps
    end
    
    execute "ALTER TABLE musicas ADD COLUMN id_musica SERIAL PRIMARY KEY;"
    execute "ALTER TABLE musicas
            ADD CONSTRAINT fk_musicas_artista
            FOREIGN KEY (id_artista)
            REFERENCES artista (id_artista)
            ON DELETE CASCADE;"

  end
end
