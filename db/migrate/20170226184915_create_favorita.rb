class CreateFavorita < ActiveRecord::Migration[5.0]


    def change
        create_table :favorita do |t|
          t.integer :id_musica
          t.integer :id_user

          t.integer :ordem
          t.timestamps
        end

        execute "
            ALTER TABLE favorita
            ADD CONSTRAINT fk_favorita_musica
            FOREIGN KEY (id_musica)
            REFERENCES musicas (id_musica)
            ON DELETE CASCADE;

            ALTER TABLE favorita
            ADD CONSTRAINT fk_favorita_user
            FOREIGN KEY (id_user)
            REFERENCES users (id_user)
            ON DELETE CASCADE;
        "

        #add_reference :musicas, index: true, foreign_key: true
        #add_reference :users, index: true, foreign_key: true
    end

end
