class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users, :primary_key=>:id_user, :id=>false do |t|
      #t.integer :id_user
      t.string :nome
      t.string :email
      t.integer :idade
      t.string :tipo_usuario
      t.string :password
      t.string :password_diggest
      t.string :password_confirmation 
      t.timestamps
    end
    execute 'ALTER TABLE "users" ADD COLUMN id_user SERIAL PRIMARY KEY;'
  end
end
