class CreateGeneros < ActiveRecord::Migration[5.0]
  def change
    create_table :generos, :primary_key=>:id_genero,:id=>false do |t|
      #t.integer :id_genero
      t.string :nome
      t.timestamps
    end
    execute "ALTER TABLE generos ADD COLUMN id_genero SERIAL PRIMARY KEY;"
  end
end
