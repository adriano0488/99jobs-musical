class CreateArtista < ActiveRecord::Migration[5.0]
  
  def change
    create_table :artista,:primary_key => :id_artista,:id=>false do |t|
      #t.integer :id_artista
      t.integer :id_genero
      t.string :nome
      t.string :thumb
      t.timestamps
      
    end

    execute "ALTER TABLE artista ADD COLUMN id_artista SERIAL PRIMARY KEY;"
    execute "ALTER TABLE artista
            ADD CONSTRAINT fk_artista_genero
            FOREIGN KEY (id_genero)
            REFERENCES generos (id_genero)
            ON DELETE CASCADE;"


       

    #add_reference :artista, index: true 
    #add_foreign_key :generos,index:true,foreign_key: true

  
  end
end
