# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


User.create({
    nome: 'Admin',
    email: 'admin@teste.com.br',
    password:   '123456',
    tipo_usuario: 'admin',
    })





Genero.create([
    {
        id_genero:1,
        nome:"Pop/ROCK Nacional"
    },
    {
        id_genero:2,
        nome:"Pagode Anos 90"
    }
    
])

#
#t.integer :id_genero
#t.string :nome
#t.string :thumb
#


Artista.create([
    {
        "id_artista":1,
        "id_genero":1,
        "nome":'Charlie Brown Jr',
        "thumb": 'http://i.imgur.com/5o1KOhd.png'
    },
    {
        "id_artista":2,
        "id_genero":1,
        "nome":'Capital Inicial',
        "thumb":"http://i.imgur.com/PrFS4ru.jpg",
    },
    
    {
        "id_artista":3,
        "id_genero":1,
        "nome":'Engenheiros do Hawaii',
        "thumb":"https://studiosol-a.akamaihd.net/letras/250x250/albuns/3/e/5/0/59491441052487.jpg",
    },
    {
        "id_artista":4,
        "id_genero":1,
        "nome":'Legião Urbana',
        "thumb":"https://upload.wikimedia.org/wikipedia/pt/thumb/e/ec/Legi%C3%A3o_Urbana_-_Mais_do_Mesmo.jpg/220px-Legi%C3%A3o_Urbana_-_Mais_do_Mesmo.jpg",
    },
    {
        "id_artista":5,
        "id_genero":2,
        "nome":'Raça Negra',
        "thumb":'http://2.bp.blogspot.com/-lbTzWRJk9vU/Uuk2xD7Q4dI/AAAAAAADjng/to4jVfLivDs/s1600/raca-negra4.jpg'
    },
    {
        "id_artista":6,
        "id_genero":2,
        "nome":'Katinguele',
        "thumb":'http://www.kboing.com.br/fotos/imagens/4a09c67768e8d.jpg'
    },
    {
        "id_artista":7,
        "id_genero":2,
        "nome":'Art Popular',
        "thumb":'http://cdn.letras.com.br/arquivos/fotos/capas/188/0018782,ao-vivo-sem-abuso.jpg'
    },
    {
        "id_artista":8,
        "id_genero":2,
        "nome":'Só pra contrariar',
        "thumb":'https://studiosol-a.akamaihd.net/letras/250x250/albuns/a/6/4/2/16481.jpg'
    }

])


Musica.create([
    {
        id_artista:1,
        nome:'Só os Loucos Sabem',
        caminho:'/assets/musicas/Charlie Brown Jr. - Só os Loucos Sabem.mp3'
    },
    {
        id_artista:1,
        nome:'Céu Azul',
        caminho:'/assets/musicas/Charlie Brown Jr. - Céu Azul (Audio Perfeito).mp3'
    },
    {
        id_artista:2,
        nome:'O Mundo',
        caminho:'/assets/musicas/capital-inicial/02 - O Mundo.mp3'
    },
    {
        id_artista:2,
        nome:'Natasha',
        caminho:'/assets/musicas/capital-inicial/12 - Natasha.mp3'
    },
    {
        id_artista:4,
        nome:'Eduardo e Mônica',
        caminho:'/assets/musicas/Legião Urbana - Eduardo e Monica.mp3'
    },

    {
        id_artista:3,
        nome:'Era Um Garoto Que Como Eu Amava Os Beatles E Os Rolling Stones',
        caminho:'/assets/musicas/Era Um Garoto Que Como Eu Amava Os Beatles E Os Rolling Stones.mp3'
    },

    {
        id_artista:5,
        nome:'Cheia de Manias',
        caminho:'/assets/musicas/Raça Negra - Cheia de Manias (Letra).mp3'
    },
    {
        id_artista:6,
        nome:'Lua Vai',
        caminho:'/assets/musicas/Katinguele - Lua Vai.mp3'
    },
    {
        id_artista:7,
        nome:'Art Popular - Pimpolho',
        caminho:'/assets/musicas/Art Popular - Pimpolho.mp3'
    },

    {
        id_artista:8,
        nome:'Que Se Chama Amor',
        caminho:'/assets/musicas/Só Pra Contrariar - Que Se Chama Amor.mp3'
    }

    
])