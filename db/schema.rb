# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170228072526) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "artista", primary_key: "id_artista", force: :cascade do |t|
    t.integer  "id_genero"
    t.string   "nome"
    t.string   "thumb"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "favorita", force: :cascade do |t|
    t.integer  "id_musica"
    t.integer  "id_user"
    t.integer  "ordem"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "generos", primary_key: "id_genero", force: :cascade do |t|
    t.string   "nome"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "musicas", primary_key: "id_musica", force: :cascade do |t|
    t.integer  "id_artista"
    t.string   "nome"
    t.string   "caminho"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", primary_key: "id_user", force: :cascade do |t|
    t.string   "nome"
    t.string   "email"
    t.integer  "idade"
    t.string   "tipo_usuario"
    t.string   "password"
    t.string   "password_diggest"
    t.string   "password_confirmation"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "password_digest"
  end

  add_foreign_key "artista", "generos", column: "id_genero", primary_key: "id_genero", name: "fk_artista_genero", on_delete: :cascade
  add_foreign_key "favorita", "musicas", column: "id_musica", primary_key: "id_musica", name: "fk_favorita_musica", on_delete: :cascade
  add_foreign_key "favorita", "users", column: "id_user", primary_key: "id_user", name: "fk_favorita_user", on_delete: :cascade
  add_foreign_key "musicas", "artista", column: "id_artista", primary_key: "id_artista", name: "fk_musicas_artista", on_delete: :cascade
end
