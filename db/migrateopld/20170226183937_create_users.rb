class CreateUsers < ActiveRecord::Migration[5.0]
  def self.up
    create_table :users do |t|
      t.integer :id_user
      t.string :nome
      t.string :email
      t.integer :idade
      t.timestamps
    end
  end

def self.down
    create_table :users, :primary_key=>:id_user, :id=>false do |t|
      t.integer :id_user
      t.string :nome
      t.string :email
      t.integer :idade
      t.timestamps
    end
  end
end
