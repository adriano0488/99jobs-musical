# LEIA

Aplicação desenvolvida em ruby on rails para favoritar músicas


Aplicação desenvolvida em:
Rails 5.0.1
ruby 2.3.3
Sistema operacional Windows 8 64 bit
Sass 3.4.23
Banco de dados PostgreSQL 9.6



Para que a aplicação funcione o deve ser feita a instalação do ruby on rails com devkit instalado corretamente.

Feito isso, clone o repositório
#PASSO A PASSO
git clone https://adriano0488-2@bitbucket.org/adriano0488-2/99jobs-musical.git

cd 99jobs-musical

rodar:
bundle install

Configurar Banco de dados em config/database.yml

development:
  adapter: postgresql
  encoding: unicode
  database: musical
  pool: 5
  username: postgres
  password: 123456

test:
  adapter: postgresql
  encoding: unicode
  database: musical
  pool: 5
  username: postgres
  password: 123456


  Rodar migration já totalmente preparada para criar todas as tabelas com os devidos relacionamentos:

  rake db:drop db:create db:migrate --trace

  Rodar seeds para registros de exemplo:

  rake db:seed --trace

#INICIAR SERVIÇO

  rails server


  A aplicação estará disponível em:

  http://localhost:3000

Usuário admin da aplicação:
login: admin@teste.com.br
senha: 123456



  O exemplo de utilização das telas está na pasta raíz do repositório: telas