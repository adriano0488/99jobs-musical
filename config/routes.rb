Rails.application.routes.draw do
    
    #get 'sessions/new'

    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    #resources :favoritas
    #resources :user
    # get '/', to: 'musicas#index', as: 'index'
    root:to => 'musicas#index'
    get    'sign_in'   => 'sessions#new'
    post   'sign_in'   => 'sessions#create'
    delete 'sign_out'  => 'sessions#destroy'
    get 'sign_out'  => 'sessions#destroy'

    get 'favoritas' =>'favoritas#index'
    

    get '/users/login' => 'users#login'

    get '/busca' => 'busca#index'

    get '/ajax/favoritar' => 'musicas#favoritar'

    post  '/admin/generos/save' => 'admin#saveGeneros'
    get '/admin/generos' => 'admin#generos'

    post  '/admin/artistas/save' => 'admin#saveArtistas'
    get '/admin/artistas' => 'admin#artistas'

    post  '/admin/musicas/save' => 'admin#saveMusicas'
    get '/admin/musicas' => 'admin#musicas'

    
    resources :users

    

    
    

end
