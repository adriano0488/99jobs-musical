module SessionsHelper
	   
  		  def sign_in(user_id) 
          session[:id_user] = @user.id_user
        end
        def current_user
            @current_user ||= User.find_by(id_user: session[:id_user])
        end
        #
        def block_access
           if current_user.present?
         		redirect_to users_path
           end
        end

        def logged_in?
              !current_user.nil?
      	end

      	def sign_out
              session.delete(:id_user)
              @current_user = nil
    	end
  
end
