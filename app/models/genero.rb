class Genero < ApplicationRecord
	
	has_many :artista,class_name:'Artista',foreign_key:"id_genero"

	self.table_name = 'generos'
    self.primary_key = 'id_genero'
end
