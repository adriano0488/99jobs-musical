class Artista < ApplicationRecord
	belongs_to :genero, foreign_key:"id_genero"
	has_many :musica, foreign_key: "id_artista"

	self.table_name = 'artista'
    self.primary_key = 'id_artista'
end
