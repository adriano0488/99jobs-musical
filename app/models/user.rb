class User < ApplicationRecord

	validates :email, 
                format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }, 
                uniqueness: true,
                unless: lambda { email.nil? }
				
				
    self.table_name = 'users'
    self.primary_key = 'id_user'

   cattr_reader :current_password


    has_secure_password
    #has_secure_password
    #validates name, presence: true, length: {maximum: 50}
    #validates password, presence: true, length: {minimum: 6}
    #VALID_EMAIL_FORMAT= /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
    #validates email, presence: true, length: {maximum: 260}, format: { with: VALID_EMAIL_FORMAT}, uniqueness: {case_sensitive: false}
	
    #before_save { 
        #self.email = email.downcase 
    #}

    def update_with_password(user_params)
        current_password = user_params.delete(:current_password)

        if self.authenticate(current_password)
          self.update(user_params)
          true
        else
          self.errors.add(:current_password, current_password.blank? ? :blank : :invalid)
          false
        end
      end

end
