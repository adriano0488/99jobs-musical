class Musica < ApplicationRecord

	belongs_to :artista, foreign_key: "id_artista"

	self.table_name = 'musicas'
    self.primary_key = 'id_musica'
end
