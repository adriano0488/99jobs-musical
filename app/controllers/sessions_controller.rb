class SessionsController < ApplicationController
  def new
  end

  before_action :block_access, except: [:destroy]
  #before_action except: [:destroy]
    def create  
              @user = User.find_by(email: params[:session][:email].downcase)
              if @user && @user.authenticate(params[:session][:password])
                  sign_in(@user)
                  redirect_to @user
              else
                flash[:notice] = "Verificar nome de usuário e senha"
                render 'new'
              end
    end


     def destroy
        sign_out
        redirect_to root_url
    end

end
