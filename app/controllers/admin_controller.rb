class AdminController < ApplicationController

	def index

	end


		#Administração de estilos musicais
	def generos

		@generos = Genero.all()


		@tipo_acao = params[:acao]

		if (@tipo_acao == 'editar')
			
			begin
				@genero = Genero.find(params[:id_genero])
			rescue Exception
				@genero = Genero.new
			end

		elsif (@tipo_acao == 'apagar')

			begin
				@genero = Genero.find(params[:id_genero])
			@genero.delete()
			rescue Exception
				
				redirect_to admin_generos_path,notice: "Ocorreu um erro ao tentar remover elemento, ele já pode ter sido removido."
				return 
			end
			
		else
			@genero = Genero.new

		end
	end

	def saveGeneros

		if(params[:genero][:nome] == '' || !params[:genero][:nome])
			redirect_to admin_generos_path,notice: "Informar nome do estilo musical"

		end

		if(params[:genero][:id_genero] == '')
			@genero = Genero.new

			#redirect_to users_path,notice: "Efetuar login"

		else

			begin
			@genero = Genero.find(params[:genero][:id_genero])
			rescue Exception
				redirect_to admin_generos_path,notice: "Genero nao encontrado"
				return
			end
		end

		g = params[:genero]

		#render :text => (params[:genero][:id_genero] == '').inspect
		#return

		@genero.nome = g[:nome]

		@genero.save()

		redirect_to admin_generos_path,notice: "Genero criado/atualizado"

		#Rails.logger.debug(params)
		#render :text => params.inspect
		

		#redirect_to '/admin/generos'

	end


###################################################################
################################MUSICAS###########################


def musicas

		@musicas = Musica.all()
		@artistas = Artista.all()



		@tipo_acao = params[:acao]

		if (@tipo_acao == 'editar')
			
			begin
				@musica = Musica.find(params[:id_musica])
			rescue Exception
				@musica = Musica.new
			end

		elsif (@tipo_acao == 'apagar')

			begin
				@musica = Musica.find(params[:id_musica])
				@musica.delete()
				redirect_to admin_musicas_path,notice: "Registro apagado"
			rescue Exception
				
				redirect_to admin_musicas_path,notice: "Ocorreu um erro ao tentar remover elemento, ele já pode ter sido removido."
			end
			return 
			
		else
			@musica = Musica.new

		end
	end

	def saveMusicas

		if(params[:musica][:nome] == '' || !params[:musica][:nome])
			redirect_to admin_musicas_path,notice: "Informar nome da Música"
			return 
		end

		if(params[:musica][:id_artista] == '' || !params[:musica][:id_artista])
			redirect_to admin_musicas_path,notice: "Selecionar artista"
			return 
		end

		if(params[:musica][:id_musica] == '')
			@musica = Musica.new

			#redirect_to users_path,notice: "Efetuar login"

		else

			begin
			@musica = Musica.find(params[:musica][:id_musica])
			rescue Exception
				redirect_to admin_musicas_path,notice: "Música não encontrada"
			end
		end

		g = params[:musica]

		#render :text => (params[:genero][:id_genero] == '').inspect
		#return

		@musica.nome = g[:nome]
		@musica.id_artista = g[:id_artista]
		@musica.caminho = g[:caminho]

		@musica.save()

		redirect_to admin_musicas_path,notice: "Música criada/atualizada"

		#Rails.logger.debug(params)
		#render :text => params.inspect
		

		#redirect_to '/admin/generos'

	end



###################################################################
################################ARTISTAS###########################


def artistas

		@artistas = Artista.all()
		@generos = Genero.all()


		@tipo_acao = params[:acao]

		if (@tipo_acao == 'editar')
			
			begin
				@artista = Artista.find(params[:id_artista])
			rescue Exception
				@artista = Artista.new
			end

		elsif (@tipo_acao == 'apagar')

			begin
				@artista = Artista.find(params[:id_artista])
				@artista.delete()
				redirect_to admin_artistas_path,notice: "Elemento removido."
			rescue Exception
				
				redirect_to admin_artistas_path,notice: "Ocorreu um erro ao tentar remover elemento, ele já pode ter sido removido."
			end
			
		else
			@artista = Artista.new

		end
	end

	def saveArtistas

		if(params[:artista][:nome] == '' || !params[:artista][:nome])
			redirect_to admin_generos_path,notice: "Informar nome do Artista"

		end

		if(params[:artista][:id_artista] == '')
			@artista = Artista.new
			a = params[:artista]

			#render :text => (params[:genero][:id_genero] == '').inspect
			#return

			if(a[:thumb] == '' || a[:thumb] == nil)
				@artista.thumb = '/assets/images/fake-thumb.jpg'
			else
				@artista.thumb = a[:thumb]
			end

			@artista.nome = a[:nome]
			@artista.id_genero = a[:id_genero]
			@result = @artista.save()
			
			


			#redirect_to users_path,notice: "Efetuar login"

		else

			begin
			@artista = Artista.find(params[:artista][:id_artista])
			rescue Exception
				redirect_to admin_generos_path,notice: "Artista nao encontrado"
				return 
			end
		end

		
		#render @artista.inspect
		
		

		redirect_to admin_artistas_path,notice: "Artista criado/atualizado"

		#Rails.logger.debug(params)
		#render :text => params.inspect
		

		#redirect_to '/admin/generos'

	end

	def artista_params
        params.require(:artista).permit(:nome,:thumb, :id_genero)
    end

end
