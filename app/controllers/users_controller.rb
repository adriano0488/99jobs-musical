class UsersController < ApplicationController
      
      before_action :authorize, except: [:new, :create]
      before_action :correct_user?, only: [ :update, :destroy]


    def index

        if session[:id_user] != nil
            id_user = session[:id_user]
            redirect_to("/users/#{id_user}")
        else
            redirect_to('/users/login')
        end
        
    end

    def new
        #attr_accessor :name, :email, :password
        #def initialize(attributes = {})
        #@name  = attributes[:name]
        #@email = attributes[:email]
        #@password = attributes[:password]
        @user = User.new
    end

    def create
        #user_p = params[:user]

        @user = User.new(user_params)
        
        if @user.save
            redirect_to @user, notice: "Usuário foi criado com sucesso! <a href='/sign_in'>Clique aqui para fazer login</a>"
            #tire o método de comentário quando criar o helper.
            #Usuário depois de cadastrar-se acessa o sistema automaticamente
            #sign_in(@user)
            #redirect_to('/users/login')
        else 
            render action: :new
        end
    end



    def update

        begin
            @user = User.find(params[:id]) 
        rescue Exception
            redirect_to users_path,notice: "Usuário não encontrado"
        end
      
        
      

     @user.update_with_password(user_params)

      if @user.update_attributes(user_params)

        redirect_to users_path,notice: "Usuário Atualizado com sucesso!"
      else
        render action: :edit
      end

    end

    def destroy
      @user = User.find(params[:id]) 
      @user.destroy
      sign_out
      redirect_to root_path
    end

     def show

         begin
            @user = User.find(params[:id]) 
        rescue Exception
            redirect_to users_path,notice: "Usuário não encontrado"
        end

      
     end

    
    private
    def user_params
        params.require(:user).permit(:nome, :email,:idade, :password, :password_confirmation)
    end

    
end
