class MusicasController < ApplicationController
    

    def index
    	@favoritas = Favorita.where(id_user:session[:id_user])
    	@musica_favoritas = [];

		@favoritas.each do |f|
			@musica_favoritas.push(f.id_musica)
		end
		


        @genero = Genero.all()
    end

    def favoritar

    	if(!params[:id_musica])
    		@result = {
	    		success:false,
	    		session:session,
	    		message:'ID da música não informado',
	    		favoritado:false
	    	}
	    	render :json => @result, content_type: "application/json"
	    	return
    	end
    	
    	#message:"Parametros de usuário e música ausentes"

    	if(session[:id_user])
    		favorita = Favorita.where(id_user:session[:id_user],id_musica:params[:id_musica])


    		#params[:id_musica]
    		removido = false


    		if(favorita.length == 0)
    			favorita = Favorita.new;
    			favorita.id_user = session[:id_user]
		    	favorita.id_musica = params[:id_musica]
		    	favorita.ordem = 0

		    	begin
            		favorita.save()	
		        rescue Exception
		        	@result = {
			    		success:true,
			    		session:session,
			    		message:'Ocorreu um erro interno do sistema',
			    		favoritado:removido
			    	}
			    	render :json => @result, content_type: "application/json"
			    	return

		            
		        end

		        message = 'Música favoritada.'
		    	
		    	

    		else
    			favorita.first().destroy();
    			removido = true
    			message = 'Música desfavoritada.'
				
    		end
    		favorita = Favorita.where(id_user:session[:id_user],id_musica:params[:id_musica])

    		
    		@result = {
	    		success:true,
	    		session:session,
	    		favorita:favorita,
	    		message:message,
	    		favoritado:!removido
	    	}
    	else
    		@result = {
	    		success:false,
	    		session:session,
	    		current_user:@current_user,
	    		message:"Usuário não autenticado, faça login para favoritar"
	    	}


    	end

     	
     	render :json => @result, content_type: "application/json"


    end

end
