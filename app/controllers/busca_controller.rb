class BuscaController < ApplicationController

	def index

		@artistas = Artista.all

		@generos = Genero.all

		@musicas= []
		@favoritas = []
		@musica_favoritas = []
		
		if session[:id_user] != nil
            id_user = session[:id_user]
            @favoritas = Favorita.where(id_user:session[:id_user])
            @musica_favoritas = [];

			@favoritas.each do |f|
				@musica_favoritas.push(f.id_musica)
			end
		end


		if(params[:tipo] == 'genero')


			@favoritas.each do |f|
				@musica_favoritas.push(f.id_musica)
			end

			@musicas = Musica.joins("inner join artista on artista.id_artista = musicas.id_artista where id_genero = #{params[:id_genero]}")
			
		elsif(params[:tipo] == 'artista')
			@musicas = Musica.where(id_artista: params[:id_artista])

		end

	end
end
